var fs = require('fs');

var instructions = fs.readFileSync('./puzzle-input.txt', 'utf8').trim().split('\r\n');
var line = [];
var grid = [];
var line2 = [];
var grid2 = [];

for (var i = 0; i < 1000; i++) {
	line.push(false);
	line2.push(0);
}

for (var i = 0; i < 1000; i++) {
	grid.push(line.slice());
	grid2.push(line2.slice());
}

instructions.forEach(function(instruction, state) {
	var instruction = instruction.split(' ');
	var action = instruction[0];
	var offset = 0;

	if (instruction[0] === 'turn') {
		action = instruction.slice(0, 2).join(' ');
		offset = 1;
	}

	var pos1 = instruction[offset + 1].split(',');
	var pos2 = instruction[offset + 3].split(',');

	for (var y = Number(pos1[1]); y <= Number(pos2[1]); y++) {
		var line = grid[y];
		var line2 = grid2[y];

		for (var x = Number(pos1[0]); x <= Number(pos2[0]); x++) {
			switch (action) {
				case 'turn on':
					line[x] = true;
					line2[x] = line2[x] + 1;
					break;
				case 'turn off':
					line[x] = false;
					line2[x] = Math.max(0, line2[x] - 1);
					break;
				case 'toggle':
					line[x] = !line[x];
					line2[x] = line2[x] + 2;
					break;
			}
		}
	}
});

var on = 0;
var intensity = 0;

for (var y = 0; y < grid.length; y++) {
	var line = grid[y];
	var line2 = grid2[y];

	for (var x = 0; x < line.length; x++) {
		if (line[x]) on = on + 1;
		intensity = intensity + line2[x];
	}
}

console.log('There are ' + on + ' lights that are on.');
console.log('The total light intensity is ' + intensity + '.');
