var fs = require('fs');

var presents = fs.readFileSync('./puzzle-input.txt', 'utf8').split('\r\n');
var totalSurface = 0;
var totalRibbon = 0;

for (var i in presents) {
	var present = presents[i];
	if (present === '') continue;

	present = present.split('x');
	var l = Number(present[0]), w = Number(present[1]), h = Number(present[2]);

	var surface = (2 * l * w) + (2 * w * h) + (2 * h * l);
	surface = surface + Math.min(l * w, w * h, h * l);

	totalSurface = totalSurface + surface;

	var sides = [l, w, h];
	var small1 = Math.min.apply(Math, sides); // We look for the smallest value in sides.
	sides.splice(sides.indexOf(small1), 1); // We remove the smallest value from sides.
	var small2 = Math.min.apply(Math, sides); // We look again for the smallest value in sides to get the second smallest value.

	var ribbon = (2 * small1) + (2 * small2);
	ribbon = ribbon + (l * w * h);

	totalRibbon = totalRibbon + ribbon;
}

console.log('The elves\' need ' + totalSurface + ' square feet of wrapping paper.');
console.log('They\'ll also need ' + totalRibbon + ' feet of ribbon.');
