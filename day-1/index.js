var fs = require('fs');

var floormap = fs.readFileSync('./puzzle-input.txt', 'utf8');
var floor = 0;
var position = 0;

for (var i = 0; i < floormap.length; i++) {
	var char = floormap[i];

	if (char === '(') {
		floor = floor + 1;
	} else if (char === ')') {
		floor = floor - 1;
	}

	if (!position && floor == -1) {
		position = i + 1;
	}
}

console.log('Santa needs to go to floor ' + floor);
console.log('He first entered floor -1 at instruction #' + position);
