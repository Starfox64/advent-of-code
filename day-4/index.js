var md5 = require('md5'); // npm i md5
var fs = require('fs');

var input = fs.readFileSync('./puzzle-input.txt', 'utf8').trim();

function findHash(input, starting) {
	console.log('Looking for hash starting with: '+ starting);
	var num = 0;

	while (true) {
		var hash = md5(input + String(num));
		if (hash.startsWith(starting)) {
			console.log('Hash found: ' + hash);
			console.log('Attempt #' + num);
			break;
		}

		num = num + 1;
	}
}

findHash(input, '00000');
findHash(input, '000000');
