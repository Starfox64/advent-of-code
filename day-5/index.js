var fs = require('fs');

var words = fs.readFileSync('./puzzle-input.txt', 'utf8').trim().split('\r\n');
var vowelChars = {a: true, e: true, i: true, o: true, u: true};
var nice1 = 0;
var nice2 = 0;

for (var i in words) {
	var word = words[i];
	var vowels = 0;
	var repeat = false;
	var pairs = [];

	for (var charI in word) {
		var char = word[charI];

		if (vowelChars[char]) vowels = vowels + 1;
		if (char === word[Number(charI) + 2]) repeat = true;

		pairs.push(word.slice(charI, Number(charI) + 2))
	}

	if (vowels >= 3 && word.match(/(.)\1/) && !word.match(/(ab|cd|pq|xy)/)) {
		nice1 = nice1 + 1;
	}

	if (repeat) {
		for (var pairI in pairs) {
			var pair = pairs[pairI];
			var isNice = false;

			for (var pairI2 in pairs) {
				if (Math.abs(pairI2 - pairI) >= 2 && pairs[pairI2] == pair) {
					isNice = true;
					break;
				}
			}

			if (isNice) {
				nice2 = nice2 + 1;
				break;
			}
		}
	}
}

console.log('Rules #1: The list contains ' + nice1 + ' nice words.');
console.log('Rules #2: The list contains ' + nice2 + ' nice words.');
