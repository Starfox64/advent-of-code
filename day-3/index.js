var fs = require('fs');

var directions = fs.readFileSync('./puzzle-input.txt', 'utf8');

function getVisitedHomes(directions, robot) {
	var santas = {
		santa: {x: 0, y: 0},
		robot: {x: 0, y: 0}
	}
	var santa = true;
	var visited = [JSON.stringify(santas.santa)];

	for (var i = 0; i < directions.length; i++) {
		var char = directions[i];
		var current = (santa) ? 'santa' : 'robot';

		switch (char) {
			case '^':
				santas[current] = {x: santas[current].x, y: santas[current].y + 1};
				break;
			case 'v':
				santas[current] = {x: santas[current].x, y: santas[current].y - 1};
				break;
			case '<':
				santas[current] = {x: santas[current].x - 1, y: santas[current].y};
				break;
			case '>':
				santas[current] = {x: santas[current].x + 1, y: santas[current].y};
				break;
		}

		var stringified = JSON.stringify(santas[current]);
		if (visited.indexOf(stringified) == -1) { // If the current pos is not in the visited array we add it.
			visited.push(stringified);
		}

		if (robot) santa = !santa;
	}

	return visited.length
}

console.log('Santa visited ' + getVisitedHomes(directions) + ' homes.');
console.log('Santa & Santa-Robot visited ' + getVisitedHomes(directions, true) + ' homes.');
